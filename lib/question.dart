class Question {
  String _question;
  bool _answer;

  Question(String question, bool answer) {
    _question = question;
    _answer = answer;
  }

  bool evalAnswer(bool a) {
    return _answer == a;
  }

  String getQuestion() {
    return _question;
  }
}
